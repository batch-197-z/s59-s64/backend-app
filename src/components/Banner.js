import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	// console.log(data)
	const { title, content, destination, destination2, label, label2 } = data;

	return(
		
		<Container id="landingPage" fluid >
		<Row className="Logo p-3"></Row>
		<Row className="justify-content-center">
			
			<Col align="center" className="p-5">
			
				<h1>{title}</h1>
				<p>{content}</p>
				
				<Col>
				
						<Button variant="light" as={Link} to={destination} ><strong>{label}</strong></Button>
				</Col>
				<Col>
						<Button variant="light" as={Link} to={destination2}><strong>{label2}</strong></Button>
				
				</Col>
			</Col>
		</Row>
		</Container>
		
	)
}
