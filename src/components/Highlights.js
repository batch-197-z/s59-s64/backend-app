import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title className="noDecor" as={Link} to="/products"><strong>Computers and Printers</strong></Card.Title>
				       <Card.Text>
				       Find PC, Laptop, Printers and other devices that suits your needs.  
				       </Card.Text>
				     </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title className="noDecor" as={Link} to="/promo"><strong>Early Christmas Deals!</strong></Card.Title>
				       <Card.Text>
				         Get up to P1,000 OFF on participating products.
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title className="noDecor" as={Link} to="/services"><strong>IT Support Services</strong></Card.Title>
				       <Card.Text>
				        	IT problem? Let our trusted IT technician help you with that!
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>
		</Row>
			
	)
};