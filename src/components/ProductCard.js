
import { useContext } from 'react';
import { Container, Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


//import Resources from './Resources'

export default function ProductCard({productProp}){

    

    const { name, description, price, _id } = productProp;
    const { user } = useContext(UserContext)
    

  return(

  
   <Container>
    <Row className="mt-3 mb-3" align="justify">
      <Col align="center" lg={{span:6, offset:3}}>
        <Card className="productCard my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
             
              
              <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
              </Card.Body>
          </Card>

          {
            (user.isAdmin) ?
                      <Button className="bg-primary" as={Link} to="/admin">Go to Admin dashboard</Button>
                      :

            (user.id !== null ) ?

          <Button className="mt-3 mb-3" align="justify-center" as={Link} to="/usersOrder">Go to user dashboard</Button>
          :
          <Button className="bg-primary" as={Link} to="/login">Log in to Order</Button>
          }
          
          
      </Col>

    </Row>
  </Container>
   
  )
}


