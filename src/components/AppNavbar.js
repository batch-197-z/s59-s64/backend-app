import React from 'react';
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';




export default function AppNavbar() {

	// useContext hook
	const { user } = useContext(UserContext);

	// getItem retrieves the data stored in the localStorage
	// const [user, setUser] = useState(localStorage.getItem("email"))
	console.log(user);

	return (

		<Navbar bg="info" expand="lg">
		 
		    <Container fluid>
		    	
		      <Navbar.Brand as={Link} to="/">HUB-bit Trading and I.T. Services</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ml-auto" >
		          <Nav.Link as={Link} to="/" >Home</Nav.Link>
		          <Nav.Link as={Link} to="/products" >Products</Nav.Link>
		          
		          {
		          	(user.id !== null) ?
		          		<Nav.Link as={Link} to="/logout" >Logout</Nav.Link>
		          		:
		          		<>
		          			<Nav.Link as={Link} to="/login" >Login</Nav.Link>
		          			<Nav.Link as={Link} to="/register" >Register</Nav.Link>
		          		</>
		          }
		          
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
	)
};
