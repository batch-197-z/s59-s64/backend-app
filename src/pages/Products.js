import { Fragment, useEffect, useState } from 'react';
// import { Button } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import ProductCard from '../components/ProductCard';


// view all active product
export default function Products() {

	const [products, setProducts] = useState([]);

	/*console.log(productsData)
	console.log(productsData[0]);*/


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />
				)
			}))

		})
	}, [])


	return (
		<Fragment>
			<h1 className="text-center my-3">Product Lists</h1>
			{products}
			
		</Fragment>

	)
}
