import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Resources from "../components/Resources";

export default function Home() {

	const data = {
		title: "HUB-bit Trading and I.T. Services",
		content: "Own and manage by Mr. and Mrs. Reyes",
		destination: "/register",
		label: "SIGN UP",
		destination2: "/login",
		label2: "Log In"
	}

	return (
		<Fragment>
			<Banner data={data} />
			<Highlights/>
			
			
		</Fragment>

	)
}