import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
// import { useParams } from 'react-router-dom';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

// retrieve single product
export default function ProductView() {

	// useContext hook for global state
	const { user } = useContext(UserContext)

	// const navigate = useNavigate();

	// const [count, setCount] = useState(0);
	// The "useParams" hook allows us to retrieve the courseId passed via URL
	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [count, setCount] = useState(1);
	const [price, setPrice] = useState(0);
	// allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling in a course. 
	 /*useHistory*/
	 console.log(count)
	
	function productView() {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setCount(count);
			setPrice(data.price);
			
		})

	}



 // checkout
	function checkout() {

		fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
			method: "POST", 
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				count: count,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			
			if(data) {
				Swal.fire({ 
				title: "Created Order successfully",
				icon: "success",
				text: "You have successfully created this order."
			}).then(reload => window.location.reload())

		

		} else {

				Swal.fire({ 
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		}
	})
};



	useEffect(() => {
		productView();
	},[productId])

 

    return (

      	<Container>
			<Row>
      			<Col align="center" lg={{span:6, offset:3}}>

        			<Card className="productCard my-3">
            			<Card.Body>
              			<Card.Title>{name}</Card.Title>
              			<Card.Subtitle>Description:</Card.Subtitle>
              			<Card.Text>{description}</Card.Text>
              			<Card.Subtitle>Price:</Card.Subtitle>
              			<Card.Text>{price}</Card.Text>
              			<div className="d-grip gap-2">
			          {
			          		(user.isAdmin) ?
              				<Button className="bg-primary" as={Link} to="/admin">Go To Admin Dashboard</Button>
              				:
			          		(user.id !== null) ?
			          			<Button onClick={() => checkout()}>Checkout</Button>
			          			:
			          			<Button className="bg-primary" as={Link} to="/login">Log in to Order</Button>
			          }
			          </div>

              			</Card.Body>
          			</Card>
          			<div>
          			{

          				(user.isAdmin) ?
          				<></>
          				:
          				user.id !== null ?
          				<Button className="mt-3 mb-3" align="justify-center" as={Link} to="/usersOrder">Go to user dashboard</Button>
          				:
          				<a className="noDecor" href="/register">No account yet? Click here to Register.</a>
          				       				

          			}
          				
          		

          			</div>
          			

          			
     
      			</Col>

    		</Row>

		</Container>

    )
}