import { Fragment, useContext, useState, useEffect} from "react";
import { Table, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext";
// import UserCard from "../components/UserCard";
import Swal from "sweetalert2";

// view all active product
export default function UsersList(){

	const {user} = useContext(UserContext);
	const [allUsers, setAllUsers] = useState([]);

	// const [firstName, setFirstName] = useState("");
	// const [lastName, setLastName] = useState("");
	// const [email, setEmail] = useState("");
	// const [mobileNo, setMobileNo] = useState("");

	/*console.log(productsData)
	console.log(productsData[0]);*/

function fetchData() {
	
// Get all users in the database
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`

			}

		})
		.then(res => res.json())
		.then(data => {
		

			setAllUsers(data.map(user => {
				return (
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "Not Admin"}</td>
						<td>
							{
								(user.isAdmin)
								?	
								 	
									<Button variant="danger" size="sm" onClick ={() => notAdmin(user._id, user.email)}>Set as Not Admin</Button>
								:
									<>
										
										<Button variant="success" size="sm" onClick ={() => admin(user._id, user.email)}>Set as Admin</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

//Making the user not admin
	const notAdmin = (userId, userEmail) =>{
		console.log(userId);
		console.log(userEmail);

		fetch(`${process.env.REACT_APP_API_URL}/users/setUserAsNotAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Set as Not Admin Succesfully!",
					icon: "success",
					text: `${userEmail} is now not an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the user as admin
	const admin= (userId, userEmail) =>{
		console.log(userId);
		console.log(userEmail);

		fetch(`${process.env.REACT_APP_API_URL}/users/setUserAsAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Set as Admin Succesfully!",
					icon: "success",
					text: `${userEmail} is now an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	useEffect(()=>{
		// invoke fetchData() to get all products.
		fetchData();
	}, [allUsers])
	


	return (
(user.isAdmin)
		?
		
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Users Information</h1>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>User ID</th>
		         <th>First Name</th>
		         <th>Last Name</th>
		         <th>Email</th>
		         <th>Mobile Number</th>
		          <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/" />
		)
}
