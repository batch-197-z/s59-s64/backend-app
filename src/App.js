// react components

import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';



// components
import AppNavbar from './components/AppNavbar';

// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
// import ProductCard from './components/ProductCard';


// pages
import AddProduct from './pages/AddProduct';
import Admin from './pages/Admin';
import EditProduct from './pages/EditProduct';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Promo from './pages/Promo';
import Register from './pages/Register';
import Services from './pages/Services';

import UsersList from './pages/UsersList';


import './App.css';

import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

const unsetUser = () => {
    localStorage.clear();
  }

useEffect(() => {

  fetch('http://localhost:4000/users/details', {
      // fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: { 
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
})
      .then(res => res.json())
      .then(data => {

          // user is logged in
          if(typeof data._id !== "undefined") {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
          } else { // user is logged out

            setUser({
              id: null,
              isAdmin: null
            })

          }
      })

  }, []);

return (

    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar/>

        <Container>
              <Routes>
                  
                  <Route path="/addProduct" element={<AddProduct/>} />
                  <Route path="/admin" element={<Admin/>} />
                  <Route path="/editProduct/:productId" element={<EditProduct/>} />
                  <Route path="*" element={<Error/>} />
                  <Route path="/" element={<Home/>} />
                  <Route path="/login" element={<Login/>} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="/products" element={<Products/>} />
                  <Route path="/products/:productId" element={<ProductView/>} />
                  <Route path="/promo" element={<Promo/>} />
                  <Route path="/register" element={<Register/>} />
                  <Route path="/services" element={<Services/>} />                 
                
                  <Route path="/usersList" element={<UsersList/>} />
                  
                  
                  <Route path="/promo" element={<Promo/>} />
                  
        
              </Routes>
            </Container>
        </Router>
    </UserProvider>

  );
}

export default App;


